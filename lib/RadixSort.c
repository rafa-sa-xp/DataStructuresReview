#include <linked_list.h>
#include <sort.h>
#include <stdlib.h>

void radix_sort(Container *c, ContainerDriver d) {
  int digit, j, k, factor;
  const int radix = 10;
  const int digits = 10;

  LinkedList *queue[radix];
  int i = 0;
  for (; i < radix; i++) {
    queue[i] = Queue().init();
  }
  for (digit = 0, factor = 1; digit < digits; factor *= radix, digit++) {
    for (j = 0; j < c->length; j++) {
      unsigned pile = (d.get_item(c, j) / factor) % radix;
      int value = d.get_item(c, j);
      Queue().insert(queue[pile], value);
    }
    for (j = 0, k = 0; j < radix; j++) {
      while (!Queue().is_empty(queue[j])) {
        int value = Queue().remove(queue[j]);
        d.set_item(c, k++, value);
      }
    }
  }

  for (; i < radix; i++) {
    Queue().destroy(queue[i]);
  }
}

SortDriver RadixSort() {
  SortDriver driver;
  driver.sort = radix_sort;
  return driver;
}