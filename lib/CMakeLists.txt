# Objects
add_library(LinkedListBase OBJECT LinkedListBase.c)
add_library(Heap OBJECT Heap.c)
add_library(Queue STATIC Queue.c $<TARGET_OBJECTS:LinkedListBase>)
#Containers
add_library(ConstArray STATIC ConstArray.c)
add_library(LinkedListContainer STATIC LinkedListContainer.c $<TARGET_OBJECTS:LinkedListBase>)
add_library(BinaryTreeBase STATIC BinaryTreeBase.c)
add_library(AVLTree STATIC AVLTree.c BinaryTreeBase)
# Array Sorting
add_library(Hello STATIC hello_world.c)
add_library(InsertionSort STATIC InsertionSort.c)
add_library(SelectionSort STATIC SelectionSort.c)
add_library(QuickSort STATIC QuickSort.c)
add_library(MergeSort STATIC MergeSort.c)
add_library(HeapSort STATIC HeapSort.c $<TARGET_OBJECTS:Heap>)
add_library(RadixSort STATIC RadixSort.c $<TARGET_OBJECTS:LinkedListBase> Queue)
# Data structures
add_library(Stack STATIC Stack.c $<TARGET_OBJECTS:LinkedListBase>)
add_library(CircularQueue STATIC CircularQueue.c $<TARGET_OBJECTS:LinkedListBase> Queue)
add_library(DoubleLinkedList STATIC DoubleLinkedList.c)