#if !defined(QuickSort_H)
#define QuickSort_H
#include <sort.h>

// TODO: Implement other pivot algorithms
unsigned get_pivot(Container *c, ContainerDriver d, unsigned first,
                   unsigned last) {
  return (first + last) / 2;
}

// NOTE: Assumes first as pivot
void quicksort_aux(Container *c, ContainerDriver d, unsigned first,
                   unsigned last) {
  unsigned lower = first + 1, upper = last;
  d.swap_item(c, first, get_pivot(c, d, first, last));
  int pivot = d.get_item(c, first);

  while (lower <= upper) {
    // Search for first element from left bigger than pivot
    while (pivot > d.get_item(c, lower)) {
      lower++;
    }
    // Search for first element from righter lesser than pivot
    while (pivot < d.get_item(c, upper)) {
      upper--;
    }
    // Check if elements should be swapped
    if (lower < upper) {
      d.swap_item(c, lower++, upper--);
    }
    // This case should happen only on last iteration
    else {
      lower++;
    }
  }
  // Put pivot before first larger number
  d.swap_item(c, first, upper);
  // Check if left part should run
  if (first < (upper - 1)) {
    quicksort_aux(c, d, first, upper - 1);
  }
  // Check if right part should run
  if ((upper + 1) < last) {
    quicksort_aux(c, d, upper + 1, last);
  }
}

// TODO: For lesser than 30 elements, it should use insertion sort
void quicksort(Container *c, ContainerDriver d) {
  int i, max;
  if (c->length < 2) {
    return;
  }
  // Optimization: Put largest item on last
  for (i = 1, max = 0; i < c->length; i++) {
    if (d.get_item(c, max) < d.get_item(c, i)) {
      max = i;
    }
  }
  d.swap_item(c, c->length - 1, max);
  quicksort_aux(c, d, 0, c->length - 2);
}

SortDriver QuickSort() {
  SortDriver driver;
  driver.sort = quicksort;
  return driver;
}
#endif  // QuickSort_H
