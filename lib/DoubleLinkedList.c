#include <container.h>
#include <stdlib.h>

typedef struct double_linked_list_node {
  struct double_linked_list_node* prev;
  struct double_linked_list_node* next;
  int value;
} DoubleLinkedNode;

typedef struct double_linked_list {
  DoubleLinkedNode* node;
} DoubleLinkedList;

static DoubleLinkedNode* newNode(int value) {
  DoubleLinkedNode* node = (DoubleLinkedNode*)malloc(sizeof(DoubleLinkedNode));
  node->next = NULL;
  node->prev = NULL;
  node->value = value;
  return node;
}

static DoubleLinkedList* init_dll() {
  DoubleLinkedList* dll = (DoubleLinkedList*)malloc(sizeof(DoubleLinkedList));
  dll->node = NULL;
  return dll;
}

static void destroy_dll(DoubleLinkedList* dll) {
  // Free all nodes
  if (dll->node != NULL) {
    DoubleLinkedNode* first = dll->node;
    DoubleLinkedNode* aux = dll->node->next;
    while (aux != first) {
      DoubleLinkedNode* temp = aux->next;
      free(aux);
      aux = temp;
    }
  }
  // Free dll
  free(dll);
}

static Container* init() {
  Container* c = (Container*)malloc(sizeof(Container));
  c->data = (void*)init_dll();
  c->length = 0;
  return c;
}

static void destroy(Container* c) {
  destroy_dll((DoubleLinkedList*)c->data);
  free(c);
}

static void add_item(Container* c, int value) {
  DoubleLinkedList* dll = (DoubleLinkedList*)c->data;
  DoubleLinkedNode* node = newNode(value);
  if (dll->node == NULL) {
    node->next = node;
    node->prev = node;
    dll->node = node;
  } else {
    node->next = dll->node;
    node->prev = dll->node->prev;
    dll->node->prev->next = node;
    dll->node->prev = node;
  }
  c->length++;
}

// 0 -> Reverse
// 1 -> Forward
static char search_forward_or_reverse(Container* c, unsigned key) {
  unsigned mid = c->length / 2;
  return key <= mid;
}

static DoubleLinkedNode* search_forward(Container* c, unsigned key) {
  DoubleLinkedNode* aux = ((DoubleLinkedList*)c->data)->node;
  while (key > 0) {
    aux = aux->next;
    key--;
  }
  return aux;
}

static DoubleLinkedNode* search_reverse(Container* c, unsigned key) {
  DoubleLinkedNode* aux = ((DoubleLinkedList*)c->data)->node;
  unsigned current = c->length - 1;
  aux = aux->prev;
  while (key != current) {
    aux = aux->prev;
    current--;
  }
  return aux;
}

static DoubleLinkedNode* get_item_closest(Container* c, unsigned key) {
  DoubleLinkedNode* aux = ((DoubleLinkedList*)c->data)->node;
  if (search_forward_or_reverse(c, key)) {
    return search_forward(c, key);
  } else {
    return search_reverse(c, key);
  }
}

static int get_item(Container* c, unsigned key) {
  assert(c != NULL);
  assert(key < c->length);

  return get_item_closest(c, key)->value;
}

static void remove_item(Container* c, unsigned key) {
  DoubleLinkedList* dll = (DoubleLinkedList*)c->data;
  DoubleLinkedNode* aux = get_item_closest(c, key);

  if (aux == dll->node) {
    dll->node = aux->next;
  }
  DoubleLinkedNode* temp = aux->next;
  aux->next->prev = aux->prev;
  aux->prev->next = temp;
  free(aux);
  c->length--;
}

static void set_item(Container* c, unsigned key, int new_value) {
  assert(c != NULL);
  assert(key < c->length);
  DoubleLinkedNode* aux = get_item_closest(c, key);
  aux->value = new_value;
}

enum SWAP_CASE { SWAP_CASE_1, SWAP_CASE_2 };

static enum SWAP_CASE get_swap_case(DoubleLinkedNode* x, DoubleLinkedNode* y) {
  // Case 1:  ... - node - x - node - ... - node - y - node - ...
  // X and Y are not next to each other
  char is_x_next_y = x->next == y;
  char is_x_prev_y = x->prev == y;

  if (is_x_next_y || is_x_prev_y) {
    // Case 2: X and Y are next to each other
    return SWAP_CASE_2;
  }
  return SWAP_CASE_1;
}

static void swap_item(Container* c, unsigned key1, unsigned key2) {
  assert(c != NULL);
  assert(key1 < c->length);
  assert(key2 < c->length);

  DoubleLinkedList* dll = (DoubleLinkedList*)c->data;
  DoubleLinkedNode* node1 = get_item_closest(c, key1);
  DoubleLinkedNode* node2 = get_item_closest(c, key2);

  // If node1 or node2 is head of the list, this should be updated
  if (node1 == dll->node) {
    dll->node = node2;
  } else if (node2 == dll->node) {
    dll->node = node1;
  }

  DoubleLinkedNode *p1, *f1, *p2, *f2;
  p1 = node1->prev;
  f1 = node1->next;
  p2 = node2->prev;
  f2 = node2->next;

  // Check which case
  enum SWAP_CASE swap_case = get_swap_case(node1, node2);
  switch (swap_case) {
    case SWAP_CASE_1: {
      node1->prev = p2;
      node1->next = f2;

      node2->prev = p1;
      node2->next = f1;
      p1->next = node2;
      f1->prev = node2;
      f2->prev = node1;
      p2->next = node1;
      break;
    }
    case SWAP_CASE_2: {
      char is_x_next_y = node1->next == node2;
      char is_x_prev_y = node1->prev == node2;

      if (is_x_next_y) {
        node1->next = f2;
        node1->prev = node2;

        node2->next = node1;
        node2->prev = p1;

        p1->next = node2;
        f2->prev = node1;
      } else {
        node1->next = node2;
        node1->prev = p2;

        node2->next = f1;
        node2->prev = node1;

        f1->prev = node2;
        p2->next = node1;
      }
      break;
    }
  }
}

ContainerDriver DoubleLinked() {
  ContainerDriver d;
  d.init = init;
  d.destroy = destroy;
  d.add_item = add_item;
  d.get_item = get_item;
  d.remove_item = remove_item;
  d.set_item = set_item;
  d.swap_item = swap_item;
  return d;
}