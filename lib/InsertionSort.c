#include <sort.h>

void insertion_sort(Container *c, ContainerDriver d)
{
    int i = 1; // Start from second element
    int j;     // For inner loop

    for (; i < c->length; i++)
    {
        int i_value = d.get_item(c, i);
        for (j = i; j > 0 && (i_value < d.get_item(c, j - 1)); j--)
        {
            d.set_item(c, j, d.get_item(c, j - 1));
        }
        d.set_item(c, j, i_value);
    }
}

SortDriver InsertionSort()
{
    SortDriver driver;
    driver.sort = insertion_sort;
    return driver;
}