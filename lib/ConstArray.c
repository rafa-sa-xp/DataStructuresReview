#include <container.h>

#define cast_data int *data = (int *)container->data

Container createConstArray(int data[], unsigned size) {
  Container array;
  array.data = data;
  array.length = size;
  return array;
}

void array_swap(Container *container, unsigned key1, unsigned key2) {
  check_length(key1, container);
  check_length(key2, container);
  cast_data;
  if (data[key1] != data[key2]) {
    int temp = data[key1];
    data[key1] = data[key2];
    data[key2] = temp;
  }
}

int array_get_item(Container *container, unsigned key) {
  check_length(key, container);
  cast_data;
  return data[key];
}

void array_set_item(Container *container, unsigned key, int new_value) {
  check_length(key, container);
  cast_data;
  data[key] = new_value;
}

#include <stdlib.h>
ContainerDriver ConstArray() {
  ContainerDriver driver;
  driver.add_item = NULL;
  driver.remove_item = NULL;
  driver.destroy = NULL;
  driver.init = NULL;
  driver.swap_item = array_swap;
  driver.get_item = array_get_item;
  driver.set_item = array_set_item;
  return driver;
}