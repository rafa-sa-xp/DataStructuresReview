#+TITLE: Tópicos Mestrado

* Introdução
** Complexidade computacional

A complexidade computacional indica o quanto de esforço é necessário ser
aplicado em um algoritmo e o quão custoso ele é. Esse custo pode ser verificado
de diversas maneiras e o contexto em particular determina seu significado como:
*tempo* e *espaço*. O fator de *tempo* geralmente é mais importante, então normalmente
há um foco maior nesse tipo de fator.

Para avaliar a eficiência de um algoritmo, unidade de tempo real (como
microssegundos e nanosegundos) não devem ser usadas. O ideal são unidades
lógicas que expressam uma relação entre o tamanho /n/ de um e uma quantitade de
tempo /t/ necessária para processa esses dados. Uma função com essa relação apenas
expressa uma medida aproximada da eficiência da função original. Entretanto,
essa aproximação é próxima o suficiente da original, especialmente para uma
função que expressa uma grande quantidade de dados. Essa medida de eficiência é
chamada de /complexidade assintótica/ e é utilizada quando alguns termos de uma
função expressão a complexidade de um algoritmo ou quando calcula-se uma função
que é difícil ou impossível e apenas aproximações podem ser encontradas. Por
exemplo:

\[ f(n) = n^2 + 100n + \log_2 n + 1000 \]


Para valores pequenos de /n/, 1000 é o maior. Quando /n/ é igual a 10, o segundo e o
terceiro termos tem o mesmo tamanho. Quando /n/ chega a 100, o primeiro e o
segundo termo tem o mesmo valor. Porém para valores maiores de 100, a
contribuição do segundo e terceiro termo se tornar menos significante. Por isso,
para valores grandes, o valor da função /f/ depende principalmente do primeiro
termo ($n^2$). Tomando o exemplo a seguir:

#+BEGIN_SRC C
  int Max (int[] vector, unsigned size) {
    int i, temp; temp = vector[0];

    for(i=2; i < size; i++) {
      if (temp < vector[i]) temp = vector[i];
    }

    return temp;
  }
#+END_SRC


Para medir o custo de execução é comum definimos uma *função de complexidade* /f/,
onde $f(n)$ é a medida de tempo necessário para um problema de tamanho /n/. Se
$f(n)$ é a medida de tempo necessário para executar um algoritmo em um problema
de tamanho /n/, então /f/ é chamada de função de *complexidade de tempo* do algoritmo.
Se $f(n)$ é uma medida de quantidade de memória necessária para executar um
algoritmo, então /f/ é chamada de função de *complexidade de espaço* do algoritmo. A
complexidade de tempo não representa o tempo diretamente, mas sim o número de
vezes que uma operação relevante é executada.

No algoritmo de *Max* que serve para encontrar o maior elemento de um vetor de
inteiros $A[0..n-1], n \geq 0$. Seja /f/ uma função de complexidade tal que $ f(n)
$ é o número de comparações entre os elementos de /A/, se /A/ tiver /n/ elementos.
Logo: \[ f(n) = n -1, \text{para } n > 0 \]

Provando que o algoritmo é *ótimo*:

*Teorema*: Qualquer algoritmo para encontrar o maior elemento de um conjunto com /n/
*elementos, $n \geq 1$ faz pelo menos $n - 1$ comparações. Prova*: Cada um dos
*$n-1$ elementos tem que ser mostrado, através de comparações, que é menor do que
*algum outro elementos, Logo $n-1$ comparações são necessárias.

O teorema nos dize que se o número de comparações for utilizado como medida de
custo então a função *Max* é ótima.

Como a medida do custo de execução de um algoritmo pode as vezes não depender
exclusivamente do tamanho de entrada é possível distinguir três cenários: melhor
caso, pior caso e caso médio. O melhor caso corresponde ao menor tempo de
execução, o *pior caso* corresponde ao maior tempo e o caso médio corresponde à
media dos tempos de execução. Na análise do caso esperado, uma distribuição de
probabilidades é suposta.

** Comportamento Assintótico
A análise de um algoritmo geralmente conta com apenas algumas operações
elementares. A medida de custo ou medida da complexidade relata o crescimento
assintótico da operação considerada.

*Definição*: Uma função $f(n)$ *domina assintoticamente* outra função $g(n)$ se
existem duas constantes positivas /c/ e /m/ tais que, para $n \geq m$, temos:
$|g(n)| \leq c \times |f(n)|$. Quando uma função domina a outra usamos a
expressão: $g(n) = O(f(n))$.

*** Notação Θ
Dada uma função $g(n)$, define-se $Θ(n)$ o conjunto de funções:

\[ Θ(g(n)) = \{ f(n): \exists c_1, \exists c_2, \exists n_0, c_1 \geq 0, c_2
\geq 0, n_0 \geq 0, 0 \leq c_1 g(n) \leq f(n) \leq c_2 g(n) \forall n \geq n_0\}
\]

Uma função $f(n)$ pertence ao conjunto $Θ(g(n))$ se existir constantes positivas
$c_1$ e $c_2$ na qual a $f(n)$ possa ficar "entre" $c_1 g(n)$ e $c_2 g(n)$. Como
$Θ(g(n))$ é um conjuntos, pode-se escrever que $f(n) \in Θ(g(n))$. Porém o uso
comum é escrever $f(n) = Θ(g(n))$
*** Notação O

A notação Θ fixa assintoticamente uma função por cima e por baixo. Quando temos
apenas por cima, é chamada de notação O.

\[ O(g(n)) = \{ f(n): \exists c, \exists n_0, c \geq 0, n_0 \geq 0, 0 \leq f(n)
\leq c g(n), \forall n \geq n_0\} \]

*** Notação Ω
*** Notação o
*** Notação ꭥ

** Exercícios
1. Expresse a função $n^3 / 1000 - 100n^2 - 100n + 3$ usando a notação-Θ

* Ordenação
** Resumo

O problema de ordenação pode ser resumido como:

*Entrada*: Uma sequência de /n/ números $(a_1,a_2,\ldots,a_n)$.

*Saída*: Uma permutação $(a'_1,a'_2,\ldots,a'_n)$ da entrada de forma que $a'_1
\leq a'_2 \leq \ldots \leq a'_n$.

Um algoritmo de ordenação pode ser estável ou não estável dependendo se ele
altera a ordem em que valores de mesmo valor apareceram. Todos os métodos não
estáveis podem ser forçados como estáveis ao agregar um índice a cada chave
antes de ordenar.

Os métodos de ordenação são classificados em dois grandes grupos. Se o arquivo a
ser ordenado cabe na memória primária, então o método é chamado de *ordenação
interna*, caso seja necessária usar a memória secundária, é chamado de *ordenação
externa*. A principal diferença é que na memória principal qualquer registro pode
ser diretamente acessado enquanto na memória secundária são acessados
sequencialmente ou em grandes blocos.

A maioria dos métodos de ordenação são baseados em *comparações*, mas também
existem métodos que usam a distribuição como base.

Tabela com todas as principais características dos principais algoritmos de
ordenação:

#+attr_latex: :align |c|c|c|c|c|
|-----------+-----------------+-----------------+---------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------|
| Algoritmo | Pior caso       | Caso médio      | Espaço                          | Estável | Resumo                                                                                                                    |
|-----------+-----------------+-----------------+---------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------|
| Insertion | $ϴ(n^2)$        | $ϴ(n^2)$        | $O(n)$ total e $O(1)$ auxiliar  | Sim     | Ordenação similar a um jogador ordenando cartas de baralho em sua mão                                                     |
|-----------+-----------------+-----------------+---------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------|
| Selection | $ϴ(n^2)$        | $ϴ(n^2)$        | $O(1)$ auxiliar                 | Não     | Itera por todos os elementos e em cada iteração busca o menor elemento                                                    |
|-----------+-----------------+-----------------+---------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------|
| Merge     | $O(n \log_2 n)$ | $O(n \log_2 n)$ | $O(n)$                          | Sim     | Separa o vetor original em n vetores pequenos e depois os une de maneira ordenada                                         |
|-----------+-----------------+-----------------+---------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------|
| Heap      | $O(n \log_2 n)$ | -               | $O(1)$                          | Não     | Constrói um /heap/ a partir de um /array/, após isso, troca a primeira posição com a última e reaplica a reconstrução do /heap/ |
|-----------+-----------------+-----------------+---------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------|
| Quick     | $O(n^2)$        | $O(n \log_2 n)$ | $O(n)$ ingênua ou $O(\log_2 n)$ | Não     | Separa o vetor original em 2 vetores com os valores menores que o pivô e o outro com os maiores.                          |
|-----------+-----------------+-----------------+---------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------|
| Radix     | $O(d(n+k))$     | $O(d(n+k))$     | $O(w+n)$                        | Sim     | Ordena com base no dígito menos significativo até o mais significativo                                                    |
|-----------+-----------------+-----------------+---------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------|

** Insertion Sort
*** Definição
É um algoritmo eficiente para ordenara um pequeno número de elementos. O
algoritmo funciona da mesma maneira que muitas pessoas ordenam as cartas na sua
mão. Começamos com a mão esquerda vazia e com as cartas viradas para baixo na
mesa. Ao removermos uma carta por vez da mesa verificamos carta por carta onde é
a posição adequada. Exemplo, ordenando a palavra *ORDENA*:

|                 | 1 | 2 | 3 | 4 | 5 | 6 |
| Chaves iniciais | *O* | R | D | E | N | A |
| i = 2           | *O* | *R* | D | E | N | A |
| i = 3           | *D* | *O* | *R* | E | N | A |
| i = 4           | *D* | *E* | *O* | *R* | N | A |
| i = 5           | *D* | *E* | *N* | *O* | *R* | A |
| i = 6           | *A* | *D* | *E* | *N* | *O* | *R* |

Um exemplo de implementação em C (para inteiros) seria:

#+BEGIN_SRC C
  void insertion_sort(int data[], unsigned n) {
    int i = 1;
    int j;
    for (; i < n; i++) {
      int tmp = data[i];
      for ( j = i; j>0 && tmp < data[j-1]; j-- )
        data[j] = data[j-1];
      data[j] = tmp;
    }
  }
#+END_SRC

Uma das vantagens desse algoritmo é que ele apenas ordena o que for realmente
necessário. Se o vetor já estiver em ordem, *basicamente nada* é feito. Uma
desvantagem é que se um elemento está sendo adicionado, todos os elementos
maiores que ele deve ser movimentados. Considerando que um elemento pode ser
movido de sua posição final apenas para ser colocado novamente.
*** Complexidade
Para encontrar o número de movimentos e comparações realizadas pelo algoritmo,
primeiro deve-se ver que o /for/ externo sempre faz $n-1$ iterações. Entretanto, o
número de elementos maiores que ~data[i]~ que serão movidos nem sempre é o mesmo.

O melhor caso é quando o vetor já esta em ordem. Apenas uma comparação é feita
para cada posição /i/, então se tem $n-1$ comparações, que é $O(n)$, e $2(n-1)$
movimentações redundantes.

O pior caso é quando o vetor esta em ordem reversa. Nesse caso, para cada
iteração i para /for/ externo são realizadas /i/ comparações, e o número total de
comparações do /loop/ é:

\[
\sum_{i=1}^{n-1} i = 1 + 2 + \ldots + (n-1) = \frac{n(n-1)}{2} = O(n^2)
\]

Para o caso médio, leva-se em consideração uma entrada randômica. O /for/ externo
*sempre* realiza $n-1$ iterações, para cada uma dessas iterações o número de
comparações depende do quão longe o item ~data[i]~ esta da sua posição correta no
sub-vetor atual. Se ele já estiver na posição correta, apenas um teste é feito
que compara ~data[i]~ com ~data[i-1]~. Se estiver a uma posição de estar correto,
duas comparações são feitas. Generalizando, se está a /j/ posições de distância do
seu local correto, ~data[i]~ é comparado com $j + 1$ outros elementos. Isso
significa que, em cada iteração de /i/ tem-se até /i/ comparações.

Assumindo probabilidade igual para os espaços do vetor, o número médio de
comparações de um elemento com os outros durante o loop pode ser computado ao
adicionar todas os números de vezes possível e dividindo pelo número de
possibilidades. O resultado é $\frac{i+1}{2}$. Ao aplicar o resultado da mesma
forma que no pior caso encontramos que o caso médio é $O(n^2)$.
*** Exercícios
1. Ilustre a aplicação do algoritmo no vetor $[31,41,59,26,41,58]$
2. Reescrever a implementação mas que dessa vez ordene de maneira decrescente

** Selection Sort
*** Definição
Tem como princípio localizar e trocar posições dos elementos das primeiras
posições até as últimas. O menor elemento é selecionado e colocado na primeira
posição, e assim sucessivamente.

|                 | 1 | 2 | 3 | 4 | 5 | 6 |
| Chaves iniciais | O | R | D | E | N | A |
| i = 2           | *A* | R | D | E | N | O |
| i = 3           | *A* | *D* | R | E | N | O |
| i = 4           | *A* | *D* | *E* | R | N | O |
| i = 5           | *A* | *D* | *E* | *N* | R | O |
| i = 6           | *A* | *D* | *E* | *N* | *O* | *R* |

Um exemplo de implementação em C (para inteiros) seria:

#+BEGIN_SRC C
  void selection_sort(int data[], unsigned n) {
    int i,j,least;
    for (i = 0; i < n-1; i++) {
      for (j = i+1, least = i; j < n; j++) {
        if (data[j] < data[least])
          least = j;
        int temp = data[least];
        data[least] = data[i];
        data[i] = temp;
      }
    }
  }
#+END_SRC

A vantagem desse algoritmo é que o único custo adicional de memória é a variável
temporária utilizada para trocar os valores.

*** Complexidade

Para analisar a performance do algoritmo, basta analisar que tem-se dois loops
com os limites bem definidos. O /loop/ externo executa $n-1$ vezes, e para cada /i/
entre $0$ e $n-2$, o /loop/ interno itera $j = (n-1) - i$ vezes. Como as
comparações são feitas no /loop/ interno, tem-se o número de comparações:

\[
\sum_{i=0}^{n-2} (n - 1 - i) = (n - 1) + \ldots + 1 = \frac{n(n-1)}{2} = O(n^2)
\]

Esse valor é o mesmo para todos os casos. Pode-se ter algumas reduções no número
de trocas. Repare que se uma atribuição durante o ~if~ for executado, apenas o índice /j/ é trocado.
Elementos do vetor são trocados incondicionalmente no loop externo enquanto o loop é executado que
é $n-1$ vezes. Logo, em todos os casos, itens são movidos o mesmo número de vezes, $3(n-1).

A melhor coisa desse algoritmos é o número total de atribuições, que basicamente não pode ser vencido por nenhum
outro algoritmo. Entretanto, pode parecer insatisfatório que o número total de trocas, é o mesmo para todos os casos.
Obviamente, nenhuma troca é necessária se o item já esta na sua posição final. O problema pode ser resolvido ao fazer
a função ~swap~ uma operação condicional.

** Quick Sort
*** Definição

O vetor original é dividido em dois sub-vetores, sendo ordenados separadamente.
Esses sub-vetores são separados sucessivamente usando o processo de partição até
que o vetor esteja ordenado.

Para particionar um vetor, precisa-se encontrar um pivô e o vetor deve ser verificado para
colocar os elementos nas posições corretas. Entretanto, encontrar um pivô ótimo não é uma tarefa trivial,
podendo desbalancear o tamanho dos sub-vetores caso um pivô não ótimo seja escolhido.

Exemplo de aplicação:

|                 | 1 | 2 | 3 | 4 | 5 | 6 |
| Chaves iniciais | O | R | D | E | N | A |
| depth = 1       | A | D | *E* | O | R | N |
| depth = 2       | *A* | D | *E* | O | N | *R* |
| depth = 3       | *A* | *D* | *E* | N | *O* | *R* |
| depth = 4       | *A* | *D* | *E* | *N* | *O* | *R* |

Exemplo de implementação:

#+BEGIN_SRC C
  void quicksort_aux(int data[], int first, int last) {
    int lower = first+1, upper = last;
    swap(data[first], data[(first+last)/2]);
    int pivot = data[first];
    while (lower <= upper) {
      while (pivot > data[lower])
        lower++;
      while (pivot < data[upper])
        upper--;
      if (lower < upper)
        swap (data[lower++], data[upper--]);
      else
        lower++;
    }
    if (first < upper-1)
      quicksort(data,first, upper-1);
    if (upper+1 < last)
      quicksort(data, upper+1, last);
  }

  void quicksort(int data[], int size) {
    int i, max;
    if (n < 2)
      return;
    for (i = 1, max = 0; i < n; i++) // find the largest
      if (data[max] < data[i])
        max = i;
    swap(data[n-1], data[max]); //largest is latest
    quicksort_aux(data, 0, n-2);
  }
#+END_SRC

*** Complexidade

O pior caso ocorre quando em cada execução do quicksort, o menor elemento do vetor é escolhido como pivô.
O algoritmo então fará as chamadas recursivas para vetores de tamanho $n-1, n-2, \ldots$ e cada um destes vetores
vai fazer $n-2 + n-3 + \ldots + 1$ comparações, e para cada partição, apenas o pivô estará na posição correta. O que
resulta numa complexidade de $O(n^2)$.

O melhor caso ocorre quando o pivô é divide um vetor em dois vetores de tamanhos iguais. Se os pivôs dos sub-vetores
forem bem escolhidos, e assim sucessivamente. O número de comparações para todas as partições é reduzido para:

\[
n + 2 \frac{n}{2} + 4 \frac{n}{4} + 8 \frac{n}{8} + \ldots + n \frac{n}{n} = n (\log n + 1)
\]

Agora para o caso médio, quando o vetor esta ordenado de maneira aleatória, como evitar o pior caso?
Para isso, precisa-se encontrar o melhor pivô possível, um dos métodos é a utilização de um número aleatório
entre o primeiro e último elemento. Esse número é utilizado como pivô. O problema é que bons geradores randômicos
custam tempo e processamento. Outro método é a utilização da mediana de 3 elementos: o primeiro, do meio e último.
Obviamente existe a possibilidade dos três elementos serem sempre os menores do vetor, mas não é muito provável.

Não é recomendado usar esse algoritmo para vetores pequenos. *Em vetores com menos de 30 itens, a ordenação por inserção é mais rápida*.
Para situações comuns de ordenação interna, o quicksort geralmente é o *melhor*.

** Merge Sort
*** Definição
Um problema com o /quick sort/ é que a complexidade do pior caso é $O(n^2)$ e é
muito difícil controlar a seleção do pivô. Os diferentes processos de seleção do
pivô tentam minimizar esse problema, porém, não há garantias. Outra estratégia é
fazer o particionamento o mais simples possível e se concentrar em unir os dois
vetores ordenados. Essa estratégia é característica do /mergesort/. Foi um dos
primeiros algoritmos utilizados em um computador

A ideia é sempre separar o vetor em dois vetores de tamanho o mais próximo
possível, e aplica-se isso recursivamente até chegar ao um vetor unitário. Então
une-se esses vetores de maneira ordenada.


*** Complexidade
Para calcular a complexidade é necessário resolver a recorrência:
\[
M(1) = 0
\]
\[
M(n) = 2M(\frac{n}{2}) + 2n
\]

O resultado é $O(n \log n)$, o pior caso tem a mesma complexidade.

Esse algoritmo pode ficar mais eficiente ao trocar a recursão por iteração ou ao
aplicar o /insertion sort/ em pequenas partes do vetor. O principal problema do
/mergesort/ é a necessidade do espaço adicionar para unir os vetores, que para
grandes quantidades de dados pode ser um obstáculo. Uma solução para isso é a
utilização de listas encadeadas.
** Heap Sort

O /selection sort/ é ineficiente pelo seu número excessivo de comparações, especialmente para um grande número de elementos. Mas ele faz poucos movimentos. A ideia do heapsort é parecida, mas diminuindo o número de comparações necessária. O /selection sort/ procura entre /n/ elementos aquele que precede todos os outros $n-1$ elementos, e assim por diante, até o vetor estar ordenado. O /heapsort/ constroi um /heap/ que é uma árvore binária com as seguintes propriedades:

1. O valor de cada nó não é menor (ou maior, dependendo do tipo escolhido) que os nós armazenados em seus filhos
2. A árvore esta balanceada e as folhas no último nível estão na posição mais a esquerda

Os elementos de um /heap/ não estão perfeitamente ordenados. É sabido apenas que o maior elemento esta no nó raiz e que para todos os outros nós, os seus descendentes não são maiores que o seu elemento pai. Então a ideia é gerar um /heap/ e colocar a raíz como o último elemento do vetor, após isso refazer o heap sem o último elemento. Exemplo de execução:

|                 | 1 | 2 | 3 | 4 | 5 | 6 |
| Chaves iniciais | O | R | D | E | N | A |
| Heap gerado     | R | O | D | E | N | A |
| i = 1           | O | N | D | E | A | *R* |
| i = 2           | N | E | D | A | *O* | *R* |
| i = 3           | E | A | D | *N* | *O* | *R* |
| i = 4           | D | A | *E* | *N* | *O* | *R* |
| i = 5           | *A* | *D* | *E* | *N* | *O* | *R* |

Exemplo de implementação:

#+BEGIN_SRC C
  void heaposrt(int data[], int n) {
    int i = n/2 - 1;
    for (; i >= 0; --i) {    // Create heap
      moveDown (data,i,n-1);
    }

    for (i = n-1; i >= 1; --i) {
      swap(data[0], data[i]); // Move largest to end
      moveDown (data, 0, i-1); // Restore heap;
    }
  }
#+END_SRC

*** Complexidade

Na primeira fase, que é a construção do heap são executados $O(n)$ passos. Na segunda fase, o algoritmo troca $n-1$ vezes o elemento raiz da posição $i$ e repara o /heap/ $n-1$ vezes, o que no pior caso faz a função iterar $\log i$ vezes, então a complexidade é:

\[
\sum_{i=1}^{n-1} (\log i) = O(n \log n)
\]

O heapsort aparenta não ser eficiente, pois as chaves são movimentadas várias vezes. Entretanto, o procedimento de restaurar o heap gasta /log n/ operações no pior caso. Logo, Heapsort gasta um tempo de execução proporcional a $n \log_2 n$ no pior caso. Esse algoritmo não é recomendado para arquivos com poucos registros, por causa do tempo necessário para a construção do /heap/. Um aspecto importante é o comportamento $O(n \log n) para qualquer que seja a entrada. Aplicações que não podem tolerar eventualmente um caso desfavorável, devem usá-lo.

No melhor caso, o vetor é composto de elementos idênticos, então a complexidade é de $O(n)$.

** Radix Sort
*** Definição
A ideia do /radix/ é a geração de pilhas de ordenação, podendo também ser
utilizado para ordenação multivalorada. Qualquer algoritmo estável pode ser
então utilizado, desde que a ordenação seja feita do valor menos significativo
ao mais significativo.
* Estruturas Simples
Um /array/ é uma estrutura de dados muito útil  porém tem duas grandes
limitações: o tamanho deve ser sabido em tempo de compilação e a data no vetor
é separada pela mesma distância na memória, o que significa que para adicionar
itens necessita de transposição de outros itens. Uma estrutura encadeada é uma
coleção de nódulos que armazenam dados e referências para outros nós. Dessa
forma eles podem ser alocados em qualquer lugar da memória.
** Listas encadeadas simples
Se um nó contém um dado que é um ponteiro para outro nó, então vários nós podem
ser anexados juntos usando apenas uma variável para acessar toda a sequência.
Esse tipo de sequência é a implementação mais comum de uma lista encadeada
simples

Um nó contém dois membros: /dado/ e /próximo/. O /dado/ é para armazenar informação, e
/próximo/ é utilizado para armazenar o próximo membro, formando a lista encadeada.

*** Pilhas

Uma estrutura do tipo *LIFO*. A operação de inserção normalmente é chamada de
~push~, e de remoção é chamada de ~pop~
*** Filas
Semelhante a pilha, porém *FIFO*. A operação de inserção normalmente é chamada de
~enqueue~ e a de remoção é chamada de ~dequeue~

*** Fila circular

Semelhante a filha, porém o último nó apontar para o primeiro, essa estrutura é
útil para situações que envolvem divisão justa (por tempo, memória, etc).
Exemplo: um /scheduler/ que define qual o processo a ser executado com base em tempo.

** Lista duplamente encadeadas
É uma estrutura parecida com uma fila circular, porém com a
** Heap
O /heap/ é uma estrutura de /array/ onde se vê o array como uma árvore binária. Com
a diferença que a única propriedade a ser mantida é que o pai deve ser
maior/menor (dependendo do tipo escolhido) do que os filhos e assim
sucessivamente.
* Árvores
Listas encadeadas geralmente tem mais flexibilidade do que vetores, porém são
estruturas lineares e são difíceis de usar para organizar uma representação
hierárquicas de objetos. Pilhas e filas contém alguma hierarquia, porém é
limitada a apenas uma dimensão. Para ir além dessa limitação, foi criado um tipo
chamar árvore que consiste em nós e arcos. Diferentemente de árvores naturais,
essas de são de cabeça para baixo, com folhas na parte de baixo e a raiz no
topo. Uma árvore pode ser definida recursivamente como:

1. Uma estrutura vazia é uma árvore vazia
2. Se $t_1,\ldots,t_k$ são árvores disjuntas, então a estrutura cuja raiz tem
   seus filhos como sendo $t_1,\ldots,t_k$, é uma árvore
3. Apenas estruturas geradas pelas regras anteriores são árvores.

Todos os nós podem ser alcançados a partir da raiz através de uma sequência
única de arcos, chamada *caminho*. O número de arcos em um caminho é chamado de
*comprimento* do caminho. O *nível* de nó é o comprimento do caminho da rais até o
nó mais um. A *altura* de uma árvore não vazia é maior nível de um nó da árvore.

Uma árvore é definida como um *grafo* /acíclico/ e /conexo/. Assim sendo, só existe um
caminho entre dois nós distintos.

** Árvore binária de busca
É uma árvore onde cada nó não tem um grau maior do que 3.  As leituras podem ser
Pré-ordem RED, em-ordem ERD, pós-ordem EDR

Nessa estrutura os nós a esquerda são menores e os a direita sõa maiores, se
balanceada a altura da árvore é igual a $\log n$

** Árvore AVL

A árvore avl é autobalanceavel utilizando a altura da árvore da esquerda menos a
da direita, se a diferença for maior que 1, então a árvore deve ser balanceada
através de rotações

*** Rotações

- *Rotação simples para a esquerda*: (A) -> (B) -> (C) => (A) <- (B) -> (C)
- *Rotação simples para a direita*: (A) <- (B) <- (C) => (A) <- (B) -> (C)
- *Rotação esquerda-direita*:  ((A) -> (B)) <- (C) => (A) <- (B) <- (C) => (A) <-
  (B) -> (C)
- *Rotação direita-esquerda*:  ((A) -> (B)) <- (C) => (A) <- (B) <- (C) => (A) <-
  (B) -> (C)


** Árvore RB
** Árvore B
