#if !defined(CONTAINER_H)
#define CONTAINER_H

/*
 *This structure will be able to hold any kind of data such as:
 * Const Array
 * Dynamic array
 * Linked lists
 * Trees
 * etc...
 */
typedef struct container {
  unsigned length;  //< Current length
  void *data;       //< Pointer to used data structure, we should not handle
                    // any transformations here
} Container;

Container createConstArray(int data[], unsigned size);
/**
 * This structure holds the needed methods to use the container api
 */
typedef struct container_driver {
  // This should initialize an default array with size 0
  Container *(*init)();
  // This should release all allocated resources
  void (*destroy)(Container *data);
  // Add an item to the struct
  void (*add_item)(Container *data, int element);
  // Remove an item
  void (*remove_item)(Container *data, unsigned key);
  // Returns integer on specific key
  int (*get_item)(Container *data, unsigned key);
  // Changes value for item on specific key
  void (*set_item)(Container *data, unsigned key, int new_value);
  // Swap values from two different keys
  void (*swap_item)(Container *data, unsigned key1, unsigned key2);
} ContainerDriver;

#include <assert.h>
#define check_length(k, c) assert(k < c->length)

ContainerDriver ConstArray();
ContainerDriver DynamicArray();
ContainerDriver LinkedListContainer();
ContainerDriver DoubleLinked();
ContainerDriver BinaryTree();
ContainerDriver AVLTree();
ContainerDriver RBTree();
ContainerDriver BTree();

#endif  // CONTAINER_H
