
#if !defined(BIN_TREE_H)
#define BIN_TREE_H

typedef enum rb_color { RB_RED, RB_BLACK } RB_COLOR;
typedef struct bin_tree_node {
  struct bin_tree_node *left;
  struct bin_tree_node *right;
  int value;
  unsigned key;
  // Color is just for RB tree be simpler to implement
  RB_COLOR color;
} BinTreeNode;

typedef struct bin_tree {
  BinTreeNode *root;

} BinTree;

int tree_height(BinTreeNode *btree);
BinTreeNode *new_binary_node(int value, unsigned key);
BinTree *new_binary_tree();
void destroy_tree(BinTree *btree);
char is_node_leaf(BinTreeNode *node);
#endif  // BIN_TREE_H
