#include <container.h>
#if !defined(HEAP_H)
#define HEAP_H

unsigned heap_parent(unsigned key);
unsigned heap_left(unsigned key);
unsigned heap_right(unsigned key);
// TODO: Create min_heapify
void max_heapify(Container *c, ContainerDriver d, unsigned smaller);
void build_max_heap(Container *c, ContainerDriver d);

#endif  // HEAP_H
