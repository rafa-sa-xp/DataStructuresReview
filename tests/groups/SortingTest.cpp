#include <CppUTest/MemoryLeakDetectorMallocMacros.h>
#include <CppUTest/MemoryLeakDetectorNewMacros.h>
#include <CppUTest/TestHarness.h>
#include "const.h"
extern "C" {
#include <sort.h>
}

void ASSERT_IS_ORDERED(Container *data, ContainerDriver d) {
  for (int i = 0; i < array_size; i++) {
    CHECK_EQUAL(array_sorted[i], d.get_item(data, i));
  }
}

TEST_GROUP(ConstArraySortingTest) {
  Container data;
  ContainerDriver d = ConstArray();
  int *data_to_sort = (int *)malloc(sizeof(int) * array_size);

  void setup() {
    data = createConstArray(data_to_sort, array_size);

    for (int i = 0; i < array_size; i++) {
      d.set_item(&data, i, array_to_sort[i]);
    }
  }

  void teardown() {
    ASSERT_IS_ORDERED(&data, d);
    free(data_to_sort);
  }
};

TEST(ConstArraySortingTest, InsertionSort) { InsertionSort().sort(&data, d); }

TEST(ConstArraySortingTest, SelectionSort) { SelectionSort().sort(&data, d); }

TEST(ConstArraySortingTest, QuickSort) { QuickSort().sort(&data, d); }

TEST(ConstArraySortingTest, MergeSort) { MergeSort().sort(&data, d); }

TEST(ConstArraySortingTest, HeapSort) { HeapSort().sort(&data, d); }

TEST(ConstArraySortingTest, RadixSort) { RadixSort().sort(&data, d); }
