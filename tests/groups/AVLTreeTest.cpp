#include <CppUTest/MemoryLeakDetectorMallocMacros.h>
#include <CppUTest/MemoryLeakDetectorNewMacros.h>
#include <CppUTest/TestHarness.h>
#include "const.h"
extern "C" {
#include <bin_tree.h>
#include <container.h>
int balance_factor(BinTree *btree);
int tree_height(BinTreeNode *btree);
}

TEST_GROUP(AVLTreeTest) { ContainerDriver d = AVLTree(); };

TEST(AVLTreeTest, tree_height) {
  ContainerDriver binary = BinaryTree();
  Container *c = binary.init();
  binary.add_item(c, 0);
  binary.add_item(c, 1);
  binary.add_item(c, 2);
  BinTree *btree = (BinTree *)c->data;
  CHECK_EQUAL(2, tree_height(btree->root));
  binary.destroy(c);
}

TEST(AVLTreeTest, balance_factor1) {
  ContainerDriver binary = BinaryTree();
  Container *c = binary.init();
  binary.add_item(c, 0);
  binary.add_item(c, 1);
  binary.add_item(c, 2);
  BinTree *btree = (BinTree *)c->data;
  CHECK_EQUAL(-2, balance_factor(btree));
  binary.destroy(c);
}
