// This file will just show how to link/test libs using cmap and cpputest

#include <CppUTest/MemoryLeakDetectorMallocMacros.h>
#include <CppUTest/MemoryLeakDetectorNewMacros.h>
#include <CppUTest/TestHarness.h>
extern "C" {
#include <bin_tree.h>
#include <container.h>
extern int balance_factor(BinTree *btree);
}

TEST_GROUP(ConstArrayTest) {
  int data[100];
  Container array = createConstArray(data, 100);
  ContainerDriver driver = ConstArray();

  void setup() {
    for (size_t i = 0; i < 100; i++) {
      data[i] = i;
    }
  }
};

TEST(ConstArrayTest, SetValue) {
  for (size_t i = 0; i < 100; i++) {
    driver.set_item(&array, i, i + 1);
  }
  for (size_t i = 0; i < 100; i++) {
    CHECK_EQUAL(i + 1, driver.get_item(&array, i));
  }
}

TEST(ConstArrayTest, GetValue) {
  for (size_t i = 0; i < 100; i++) {
    CHECK_EQUAL(i, driver.get_item(&array, i));
  }
}

TEST(ConstArrayTest, SwapValue) {
  // Reverting array
  for (size_t i = 0; i < 50; i++) {
    driver.swap_item(&array, i, 99 - i);
  }

  for (size_t i = 0; i < 100; i++) {
    CHECK_EQUAL(99 - i, driver.get_item(&array, i));
  }
}

TEST_GROUP(LinkedListContainerTest) {
  Container *c;
  ContainerDriver driver = LinkedListContainer();

  void setup() {
    c = driver.init();
    for (size_t i = 0; i < 100; i++) {
      driver.add_item(c, i);
    }
  }

  void teardown() { driver.destroy(c); }
};

TEST(LinkedListContainerTest, Init) {
  Container *test = driver.init();
  int expectedSize = 0;
  int actualSize = test->length;
  CHECK_EQUAL(expectedSize, actualSize);
  driver.destroy(test);
}

TEST(LinkedListContainerTest, AddItemShouldIncreaseLength) {
  Container *test = driver.init();
  driver.add_item(test, 0);
  int expectedSize = 1;
  int actualSize = test->length;
  CHECK_EQUAL(expectedSize, actualSize);
  driver.destroy(test);
}

TEST(LinkedListContainerTest, AddItemShouldSetValue) {
  Container *test = driver.init();
  int expectedValue = 7;

  driver.add_item(test, expectedValue);
  int actualValue = driver.get_item(test, 0);
  CHECK_EQUAL(expectedValue, actualValue);
  driver.destroy(test);
}

TEST(LinkedListContainerTest, RemoveItemShouldDecreaseLength) {
  Container *test = driver.init();
  driver.add_item(test, 0);
  driver.remove_item(test, 0);
  int expectedSize = 0;
  int actualSize = test->length;
  CHECK_EQUAL(expectedSize, actualSize);
  driver.destroy(test);
}

TEST(LinkedListContainerTest, SetValue) {
  for (size_t i = 0; i < 100; i++) {
    driver.set_item(c, i, i + 1);
  }
  for (size_t i = 0; i < 100; i++) {
    CHECK_EQUAL(i + 1, driver.get_item(c, i));
  }
}

TEST(LinkedListContainerTest, GetValue) {
  for (size_t i = 0; i < 100; i++) {
    CHECK_EQUAL(i, driver.get_item(c, i));
  }
}

TEST(LinkedListContainerTest, SwapValue) {
  // Reverting array
  for (size_t i = 0; i < 50; i++) {
    driver.swap_item(c, i, 99 - i);
  }

  for (size_t i = 0; i < 100; i++) {
    CHECK_EQUAL(99 - i, driver.get_item(c, i));
  }
}

TEST_GROUP(DoubleLinkedTest) {
  Container *c;
  ContainerDriver driver = DoubleLinked();

  void setup() {
    c = driver.init();
    for (size_t i = 0; i < 100; i++) {
      driver.add_item(c, i);
    }
  }

  void teardown() { driver.destroy(c); }
};

TEST(DoubleLinkedTest, Init) {
  Container *test = driver.init();
  int expectedSize = 0;
  int actualSize = test->length;
  CHECK_EQUAL(expectedSize, actualSize);
  CHECK_TRUE(test->data != NULL);
  driver.destroy(test);
}

TEST(DoubleLinkedTest, AddItemShouldIncreaseLength) {
  Container *test = driver.init();
  driver.add_item(test, 0);
  int expectedSize = 1;
  int actualSize = test->length;
  CHECK_EQUAL(expectedSize, actualSize);
  driver.destroy(test);
}

TEST(DoubleLinkedTest, AddItemShouldSetValue) {
  Container *test = driver.init();
  int expectedValue = 7;

  driver.add_item(test, expectedValue);
  int actualValue = driver.get_item(test, 0);
  CHECK_EQUAL(expectedValue, actualValue);
  driver.destroy(test);
}

TEST(DoubleLinkedTest, RemoveItemShouldDecreaseLength) {
  Container *test = driver.init();
  driver.add_item(test, 0);
  driver.remove_item(test, 0);
  int expectedSize = 0;
  int actualSize = test->length;
  CHECK_EQUAL(expectedSize, actualSize);
  driver.destroy(test);
}

TEST(DoubleLinkedTest, RemoveItemShouldShiftItems) {
  Container *test = driver.init();
  driver.add_item(test, 0);
  driver.add_item(test, 1);
  driver.remove_item(test, 0);
  int expectedValue = 1;
  int actualValue = driver.get_item(test, 0);
  CHECK_EQUAL(expectedValue, actualValue);
  driver.destroy(test);
}

TEST(DoubleLinkedTest, SetValue) {
  for (size_t i = 0; i < 100; i++) {
    driver.set_item(c, i, i + 1);
  }
  for (size_t i = 0; i < 100; i++) {
    CHECK_EQUAL(i + 1, driver.get_item(c, i));
  }
}

TEST(DoubleLinkedTest, GetValue) {
  for (size_t i = 0; i < 1; i++) {
    CHECK_EQUAL(i, driver.get_item(c, i));
  }
}

TEST(DoubleLinkedTest, CheckSwapEdge1) {
  Container *test = driver.init();
  for (int i = 0; i < 10; i++) {
    driver.add_item(test, i);
  }
  driver.swap_item(test, 3, 7);
  int expectedValue = 7;
  int actualValue = driver.get_item(test, 3);
  CHECK_EQUAL(expectedValue, actualValue);
  driver.destroy(test);
}

TEST(DoubleLinkedTest, CheckSwapEdge2) {
  Container *test = driver.init();
  for (int i = 0; i < 10; i++) {
    driver.add_item(test, i);
  }
  driver.swap_item(test, 7, 6);
  int expectedValue = 7;
  int actualValue = driver.get_item(test, 6);
  CHECK_EQUAL(expectedValue, actualValue);
  driver.destroy(test);
}

TEST(DoubleLinkedTest, SwapValue) {
  // Reverting array
  for (size_t i = 0; i < 50; i++) {
    driver.swap_item(c, i, 99 - i);
  }

  for (size_t i = 0; i < 100; i++) {
    CHECK_EQUAL(99 - i, driver.get_item(c, i));
  }
}

TEST_GROUP(BinaryTreeTest) {
  Container *c;
  ContainerDriver driver = BinaryTree();
  void setup() {
    c = driver.init();
    for (size_t i = 0; i < 100; i++) {
      driver.add_item(c, i);
    }
  }

  void teardown() { driver.destroy(c); }
};

TEST(BinaryTreeTest, Init) {
  Container *test = driver.init();
  int expectedSize = 0;
  int actualSize = test->length;
  CHECK_EQUAL(expectedSize, actualSize);
  CHECK_TRUE(test->data != NULL);
  driver.destroy(test);
}

TEST(BinaryTreeTest, AddItemShouldIncreaseLength) {
  Container *test = driver.init();
  driver.add_item(test, 0);
  int expectedSize = 1;
  int actualSize = test->length;
  CHECK_EQUAL(expectedSize, actualSize);
  driver.destroy(test);
}

TEST(BinaryTreeTest, AddItemShouldSetValue) {
  Container *test = driver.init();
  int expectedValue = 7;

  driver.add_item(test, expectedValue);
  int actualValue = driver.get_item(test, 0);
  CHECK_EQUAL(expectedValue, actualValue);
  driver.destroy(test);
}

TEST(BinaryTreeTest, AddMultipleValues) {
  Container *test = driver.init();
  int expectedValue = 7;

  driver.add_item(test, 0);
  driver.add_item(test, expectedValue);
  driver.add_item(test, 9);
  int actualValue = driver.get_item(test, 1);
  CHECK_EQUAL(expectedValue, actualValue);
  driver.destroy(test);
}

TEST(BinaryTreeTest, RemoveItemShouldDecreaseLength) {
  Container *test = driver.init();
  driver.add_item(test, 0);
  driver.remove_item(test, 0);
  int expectedSize = 0;
  int actualSize = test->length;
  CHECK_EQUAL(expectedSize, actualSize);
  driver.destroy(test);
}

TEST(BinaryTreeTest, RemoveItemShouldShiftItems) {
  Container *test = driver.init();
  driver.add_item(test, 0);
  driver.add_item(test, 1);
  driver.remove_item(test, 0);
  int expectedValue = 1;
  int actualValue = driver.get_item(test, 0);
  CHECK_EQUAL(expectedValue, actualValue);
  driver.destroy(test);
}

TEST(BinaryTreeTest, RemoveFromLeaf) {
  Container *test = driver.init();
  for (int i = 0; i < 50; i++) driver.add_item(test, i);

  driver.remove_item(test, 49);
  int expectedValue = 48;
  int actualValue = driver.get_item(test, 48);
  CHECK_EQUAL(expectedValue, actualValue);
  driver.destroy(test);
}

TEST(BinaryTreeTest, RemoveFromMidle) {
  Container *test = driver.init();
  for (int i = 0; i < 50; i++) driver.add_item(test, i);

  driver.remove_item(test, 25);
  int expectedValue = 26;
  int actualValue = driver.get_item(test, 25);
  CHECK_EQUAL(expectedValue, actualValue);
  driver.destroy(test);
}

TEST(BinaryTreeTest, SetValue) {
  for (size_t i = 0; i < 100; i++) {
    driver.set_item(c, i, i + 1);
  }
  for (size_t i = 0; i < 100; i++) {
    CHECK_EQUAL(i + 1, driver.get_item(c, i));
  }
}

TEST(BinaryTreeTest, GetValue) {
  for (size_t i = 0; i < 1; i++) {
    CHECK_EQUAL(i, driver.get_item(c, i));
  }
}

TEST(BinaryTreeTest, SwapValue) {
  // Reverting array
  for (size_t i = 0; i < 50; i++) {
    driver.swap_item(c, i, 99 - i);
  }

  for (size_t i = 0; i < 100; i++) {
    CHECK_EQUAL(99 - i, driver.get_item(c, i));
  }
}

TEST_GROUP(AVLTreeTest) {
  Container *c;
  ContainerDriver driver = AVLTree();
  void setup() {
    c = driver.init();
    for (size_t i = 0; i < 6; i++) {
      driver.add_item(c, i);
    }
  }

  void teardown() { driver.destroy(c); }
};

TEST(AVLTreeTest, Init) {
  Container *test = driver.init();
  int expectedSize = 0;
  int actualSize = test->length;
  CHECK_EQUAL(expectedSize, actualSize);
  CHECK_TRUE(test->data != NULL);
  driver.destroy(test);
}

TEST(AVLTreeTest, AddItemShouldIncreaseLength) {
  Container *test = driver.init();
  driver.add_item(test, 0);
  int expectedSize = 1;
  int actualSize = test->length;
  CHECK_EQUAL(expectedSize, actualSize);
  driver.destroy(test);
}

TEST(AVLTreeTest, AddItemShouldSetValue) {
  Container *test = driver.init();
  int expectedValue = 7;

  driver.add_item(test, expectedValue);
  int actualValue = driver.get_item(test, 0);
  CHECK_EQUAL(expectedValue, actualValue);
  driver.destroy(test);
}

TEST(AVLTreeTest, AddMultipleValues) {
  Container *test = driver.init();
  int expectedValue = 7;

  driver.add_item(test, 0);
  driver.add_item(test, expectedValue);
  driver.add_item(test, 9);
  int actualValue = driver.get_item(test, 1);
  CHECK_EQUAL(expectedValue, actualValue);
  driver.destroy(test);
}

TEST(AVLTreeTest, AddMultipleValuesShouldBalanceEdge1) {
  Container *test = driver.init();

  driver.add_item(test, 0);
  driver.add_item(test, 5);
  driver.add_item(test, 9);
  int expectedValue = 0;
  BinTree *btree = (BinTree *)test->data;
  int actualValue = balance_factor(btree);
  CHECK_EQUAL(expectedValue, actualValue);
  driver.destroy(test);
}

TEST(AVLTreeTest, AddMultipleValuesShouldBalanceEdge2) {
  Container *test = driver.init();

  driver.add_item(test, 0);
  driver.add_item(test, 5);
  driver.add_item(test, 4);
  int expectedValue = 0;
  BinTree *btree = (BinTree *)test->data;
  int actualValue = balance_factor(btree);
  CHECK_EQUAL(expectedValue, actualValue);
  driver.destroy(test);
}

TEST(AVLTreeTest, AddMultipleValuesShouldBalanceEdge3) {
  Container *test = driver.init();

  driver.add_item(test, 9);
  driver.add_item(test, 5);
  driver.add_item(test, 0);
  int expectedValue = 0;
  BinTree *btree = (BinTree *)test->data;
  int actualValue = balance_factor(btree);
  CHECK_EQUAL(expectedValue, actualValue);
  driver.destroy(test);
}
