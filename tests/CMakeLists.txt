
find_package(PkgConfig REQUIRED)
pkg_search_module(CPPUTEST REQUIRED cpputest)
message(STATUS "Found CppUTest version ${CPPUTEST_VERSION}")


set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

add_library(ContainerTest STATIC ./groups/ContainerTest.cpp)
add_library(SortingTest STATIC ./groups/SortingTest.cpp )
add_library(LinkedListTest STATIC ./groups/LinkedListTest.cpp)
add_library(AVLTreeTest STATIC ./groups/AVLTreeTest.cpp)

add_executable(RunAllTests RunAllTests.cpp)
target_link_libraries(RunAllTests
    ContainerTest
    SortingTest
    LinkedListTest
    AVLTreeTest
    ${CPPUTEST_LDFLAGS}
    ConstArray
    LinkedListContainer
    DoubleLinkedList
    Stack
    Queue
    CircularQueue
    InsertionSort
    SelectionSort
    QuickSort
    MergeSort
    HeapSort
    RadixSort
    BinaryTreeBase
    AVLTree
)

add_executable(Playground playground.c)
target_link_libraries(Playground
    ConstArray
    LinkedListContainer
    DoubleLinkedList
    Stack
    Queue
    CircularQueue
    InsertionSort
    SelectionSort
    QuickSort
    MergeSort
    HeapSort
    RadixSort
    BinaryTreeBase
    AVLTree
)
message("Adding tests: ${TestSuite}")
# add_test(NAME ConstArrayTests COMMAND RunAllTests -g ConstArrayTest)
# add_test(NAME LinkedListContainerTests COMMAND RunAllTests -g LinkedListContainerTest)
# add_test(NAME ConstArraySortingTest COMMAND RunAllTests -g ConstArraySortingTest)
# add_test(NAME StackTest COMMAND RunAllTests -g StackTest)
# add_test(NAME QueueTest COMMAND RunAllTests -g QueueTest)
# add_test(NAME CircularQueueTest COMMAND RunAllTests -g CircularQueueTest)
# add_test(NAME DoubleLinkedTest COMMAND RunAllTests -g DoubleLinkedTest)
add_test(NAME BinaryTreeTest COMMAND RunAllTests -g BinaryTreeTest)
add_test(NAME AVLTreeTest COMMAND RunAllTests -g AVLTreeTest)
if (TestSuite STREQUAL Full)
    message("Adding full tests")
    add_test(NAME LinkedListContainerSortingTest COMMAND RunAllTests -g LinkedListContainerSortingTest)
endif()
